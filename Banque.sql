DROP TABLE IF EXISTS operations;
DROP TABLE IF EXISTS solde;
DROP TABLE IF EXISTS cat;

CREATE TABLE `solde`(
    `soldeid`INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `total` FLOAT(10) NOT NULL,
    `date-start` DATE NOT NULL,
    `date-end` DATE NOT NULL


    );

CREATE TABLE `cat` (
    `catid`INT NOT NULL AUTO_INCREMENT  PRIMARY KEY,
    `title` VARCHAR (200) 
) ;

CREATE TABLE `operations` (
    `operaid` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `title` VARCHAR (200),
    `total` FLOAT (10) NOT NULL,
    `date` DATE NOT NULL,
    `soldeid` INT,
    `catid` INT ,
    CONSTRAINT `fk_soldeid` FOREIGN KEY (`soldeid`) REFERENCES `solde` (`soldeid`) ON DELETE SET NULL,
    CONSTRAINT `fk_catid` FOREIGN KEY (`catid`) REFERENCES `cat` (`catid`) ON DELETE SET NULL 


);


