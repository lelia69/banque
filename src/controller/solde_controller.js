import { Router } from "express";
import { SoldeRepository } from "../repository/soldeRepository";

export const soldeController = Router();

soldeController.get('/all', async (req,res)=>{
    let result = await new SoldeRepository().totalAll()
    res.json(result)
})

soldeController.get('/select/:id', async (req,res)=>{
    let result = await new SoldeRepository().findByid(req.params.id)
    res.json(result)
})

soldeController.delete('/delete/:id', async (req,res)=>{
    await new SoldeRepository().delete(req.params.id)
    res.end()
})

soldeController.put('/update', async (req,res)=>{
    await new SoldeRepository().update(req.body)
    res.end()
})

soldeController.post('/add', async(req,res)=>{
    await new SoldeRepository().add(req.body)
    res.end()
})