import {Router} from "express";
import { catRepository } from "../repository/catRepository";

export const catController = Router();

catController.get('/all', async (req,res) =>{
    let result = await new catRepository().findAll();
    res.json(result)
})

catController.get('/select/:id', async (req,res) =>{
    let result = await new catRepository().findByid(req.params.id);
    res.json(result)
})

catController.post('/add', async (req,res)=>{
    await new catRepository().add(req.body);
    res.end()
})

catController.put('/update', async (req,res)=>{
     await new catRepository().update(req.body);
    res.end()
})

catController.delete('/delete/:id', async (req, res) => {
    await new catRepository().delete(req.params.id);
    res.end();
})

