import { response, Router } from "express";
import { operationRepository } from "../repository/operationRepository";


export const operationsController = Router();

operationsController.get('/all', async (req,res)=>{
    let result = await new operationRepository().findAll();
    res.json(result)
});

operationsController.get('/select/:id', async (req,res)=>{
    let result = await new operationRepository().findByid(req.params.id);
    res.json(result)
})

operationsController.put('/update', async(req,res)=>{
    await new operationRepository().update(req.body);
    res.end()
})
operationsController.delete('/delete/:id', async (req,res) => {
    await new operationRepository().delete(req.params.id);
    res.end();
})


operationsController.post('/add', async(req,res)=>{
    try{
        await new operationRepository().add(req.body)
    res.end()
    } catch(error){
        console.log(error);
        response.status(500).json(error)
    }
    
})
