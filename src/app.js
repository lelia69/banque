import 'dotenv-flow/config';
import { server } from './server';

let port = process.env.PORT || 7000;

server.listen(port, ()=>{
    console.log('server is running on port '+port);
})


