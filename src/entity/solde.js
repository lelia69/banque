export class Solde {
    soldeid;
    total;
    date_start;
    date_end;

    constructor(soldeid, total, date_start,date_end) {
        this.soldeid = soldeid;
        this.total = total;
        this.date_start = date_start;
        this.date_end = date_end;

    }
}