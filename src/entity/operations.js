export class Operations {
    operaid;
    title;
    total;
    date;
    catid;
    

    constructor(operaid, title, total, date, catid) {
        this.operaid = operaid;
        this.title = title;
        this.total = total;
        this.date = date;
        this.catid = catid;
        
    }
}