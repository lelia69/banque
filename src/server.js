import express from "express";
import { catController } from "./controller/cat_controller";
import { operationsController } from "./controller/operations_controller";
import { soldeController } from "./controller/solde_controller";
import cors from 'cors';


export const server = express();


server.use(express.json());
server.use(express.urlencoded({extended: true}))
server.use(cors());

server.use('/api/cat', catController)
server.use('/api/operations', operationsController)
server.use('/api/solde', soldeController)