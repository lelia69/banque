import { Solde } from "../entity/solde";
import { connection } from "./connection";

export class SoldeRepository{

    async totalAll(){
        let [rows] = await connection.execute('SELECT * from solde')
        let solde = [];
        for (const row of rows) {
            let instance = new Solde(row.soldeid, row.total, row.date_start, row.date_end);
            solde.push(instance);
        }
        return solde
    }

    async findByid(id){
        let [rows] = await connection.execute('SELECT * from solde WHERE soldeid = ?' ,[id])
        let solde = [];
        for(const row of rows){
            let instance = new Solde(row.soldeid, row.total, row.date_start, row.date_end);
            solde.push(instance);
        }
        return solde
    } 

    async delete(id){
        const [rows] = await connection.execute( 'DELETE from solde WHERE soldeid=?', [id])
    }

    async update(params){
        const [rows] = await connection.execute('UPDATE solde SET total=? , date_start=? , date_end= ? WHERE soldeid=?' [params.total, params.date_start, params.date_end, params.soldeid])
    }

    async add(solde){
        let [data]= await connection.execute('INSERT INTO solde (total,date_start,date_end) VALUES (?,?,?)', [solde.total, solde.date_start,solde.date_end]);
        return solde.id = data.insertId
    }
}