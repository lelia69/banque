import { Operations } from "../entity/operations";
import { connection } from "./connection.js";


export class operationRepository {
    async findAll() {
        let [rows] = await connection.execute('SELECT * from operations' )
        let ope = [];
        for (const row of rows) {
            let instance = new Operations(row.operaid, row.title, row.total, row.date, row.catid);
            ope.push(instance);
        }
        return ope
    }

    async findByid(id){
        let [rows] = await connection.execute('SELECT * from operations WHERE operaid = ?' ,[id])
        let ope = [];
        for(const row of rows){
            let instance = new Operations(row.operaid, row.title, row.total, row.date, row.catid);
            ope.push(instance);
        }
        return ope
    } 


    async update(id){
        let [rows] = await connection.execute('UPDATE operations SET title=?, total=?, date=?, catid=? WHERE operaid= ?', [id.title,id.total,id.date,id.catid,id.operaid]);

    }

    async delete(id){
        let [rows] = await connection.execute('DELETE FROM operations WHERE operaid= ?', [id])
    }

    
    async add(ope){
        let [data]= await connection.execute('INSERT INTO operations (title,total,date,catid) VALUES (?,?,?,?)', [ope.title,ope.total,ope.date,ope.catid]);
        return ope.id = data.insertId
    }
}

