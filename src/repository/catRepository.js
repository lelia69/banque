import {connection} from "./connection.js";
import { Cat } from "../entity/cat";

export class catRepository {
    async findAll(){

        let [rows] = await connection.execute('SELECT * from cat')
        let cat = [];
        for (const row of rows) {
            let instance = new Cat(row.catid, row.title);
            cat.push(instance);
        }
        return cat
    }

    async findByid(id){
        let [rows] = await connection.execute('SELECT * from cat WHERE catid = ?' ,[id])
        let cat = [];
        for(const row of rows){
            let instance = new Cat(row.catid, row.title);
            cat.push(instance);
        }
        return cat
    } 

    async update(updateCat){
        const [rows] = await connection.execute('UPDATE cat SET title=? WHERE catid=?', [updateCat.title,updateCat.catid])
    }
     
    async delete(id){
        const [rows] = await connection.execute('DELETE FROM cat WHERE catid=?', [id])
    }

    async add(cat){
        let [data]= await connection.execute('INSERT INTO cat (title) VALUES (?)', [cat.title]);
        return cat.id = data.insertId
    }
    
    }
