/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/ BANQUE /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE BANQUE;

DROP TABLE IF EXISTS cat;
CREATE TABLE `cat` (
  `catid` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`catid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS operations;
CREATE TABLE `operations` (
  `operaid` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `total` float NOT NULL,
  `date` date NOT NULL,
  `catid` int(11) DEFAULT NULL,
  PRIMARY KEY (`operaid`),
  KEY `fk_catid` (`catid`),
  CONSTRAINT `fk_catid` FOREIGN KEY (`catid`) REFERENCES `cat` (`catid`) ON DELETE SET NULL ON UPDATE CASCADE 
 
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS solde;
CREATE TABLE `solde` (
  `soldeid` int(11) NOT NULL AUTO_INCREMENT,
  `total` float NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  PRIMARY KEY (`soldeid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

INSERT INTO cat(catid,title) VALUES(1,'RESTAURANTS'),(2,'MAISON'),(3,'VINS');

INSERT INTO operations(operaid,title,total,date,catid) VALUES(1,'Tamtamerie',150,'2021-06-23',NULL),(2,'Loyer',650,'2021-06-07',NULL),(3,'Gevrey-Chambertin',60,'2021-06-18',1);
INSERT INTO solde(soldeid,total,date_start,date_end) VALUES(1,900,'2021-06-01','2021-06-30');